//
//  Car.h
//  DelegatesDemo
//
//  Created by James Cash on 08-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

// Higher-lever concerns that our car needs to ask its driver to decide
@protocol CarDelegate <NSObject>

- (BOOL)canDrive;

@optional

- (NSInteger)howFastShouldIGoOnTheHighway;

- (BOOL)stopDrivingWhenYouSee:(NSString*)hazard;

@end

@interface Car : NSObject

@property (weak,nonatomic) id<CarDelegate> delegate;

- (instancetype)initWithDriver:(id<CarDelegate>)driver;
// low-level car methods that it knows how to handle - the details of driving
- (void)goForDrive;

@end
