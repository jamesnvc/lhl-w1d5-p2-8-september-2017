//
//  Car.m
//  DelegatesDemo
//
//  Created by James Cash on 08-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "Car.h"

@implementation Car

- (instancetype)initWithDriver:(id<CarDelegate>)driver
{
    self = [super init];
    if (self) {
        _delegate = driver;
    }
    return self;
}

- (void)goForDrive {
    // This also doubles as checking we have a delegate
    // because if self.delegate is nil, then any method on nil returns nil
    // and nil is considered "falsy", so [nil canDrive] is false
    // so ![nil canDrive] is true
    // so if self.delegate is nil, the car won't start
    if (![self.delegate canDrive]) {
        NSLog(@"Can't start without a good driver!");
        return;
    }
    NSLog(@"Starting");

    NSLog(@"getting on the highway");
    NSInteger speed = 100;
    if ([self.delegate respondsToSelector:@selector(howFastShouldIGoOnTheHighway)]) {
        speed = [self.delegate howFastShouldIGoOnTheHighway];
    }
    NSLog(@"Going at %ld km/h", speed);

    BOOL hasHazardOpinions = [self.delegate respondsToSelector:@selector(stopDrivingWhenYouSee:)];
    for (NSString *hazard in @[@"rain", @"sleet", @"white-out"]) {
        if (hasHazardOpinions && [self.delegate stopDrivingWhenYouSee:hazard]) {
            NSLog(@"Oh no, encountered %@, pulling over until it passes", hazard);
            return;
        }
        NSLog(@"Driving through %@", hazard);
    }

    NSLog(@"Arrived!");

}



@end
