//
//  FastDriver.m
//  DelegatesDemo
//
//  Created by James Cash on 08-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "FastDriver.h"

@implementation FastDriver

- (BOOL)canDrive
{
    return YES;
}

- (NSInteger)howFastShouldIGoOnTheHighway
{
    return 200;
}

- (BOOL)stopDrivingWhenYouSee:(NSString *)hazard
{
    if ([hazard isEqualToString:@"white-out"]) {
        return YES;
    }
    if ([hazard isEqualToString:@"snow"]) {
        return YES;
    }
    return NO;
}

@end
