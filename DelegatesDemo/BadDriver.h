//
//  BadDriver.h
//  DelegatesDemo
//
//  Created by James Cash on 08-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Car.h"

@interface BadDriver : NSObject <CarDelegate>

@end
