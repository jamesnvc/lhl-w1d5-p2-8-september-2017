//
//  RecklessDriver.m
//  DelegatesDemo
//
//  Created by James Cash on 08-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "NormalDriver.h"

@implementation NormalDriver

- (BOOL)canDrive {
    return YES;
}

@end
