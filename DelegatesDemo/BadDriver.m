//
//  BadDriver.m
//  DelegatesDemo
//
//  Created by James Cash on 08-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "BadDriver.h"

@implementation BadDriver

- (BOOL)canDrive {
    return NO;
}

@end
