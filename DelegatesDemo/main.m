//
//  main.m
//  DelegatesDemo
//
//  Created by James Cash on 08-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Car.h"
#import "BadDriver.h"
#import "NormalDriver.h"
#import "FastDriver.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        BadDriver *bad = [[BadDriver alloc] init];
        Car *someCar = [[Car alloc] initWithDriver:bad];
        NSLog(@"BAD DRIVER");
        [someCar goForDrive];

        NormalDriver *norm = [[NormalDriver alloc] init];
        someCar.delegate = norm;
        NSLog(@"NORMAL DRIVER");
        [someCar goForDrive];

        someCar.delegate = nil;
        NSLog(@"NO DRIVER");
        [someCar goForDrive];

        FastDriver *furiosa = [[FastDriver alloc] init];
        someCar.delegate = furiosa;
        NSLog(@"ONCE AGAIN, WE RIDE");
        [someCar goForDrive];


        // NSNumber
//        [@(1) intValue] + [@(2) intValue];
    }
    return 0;
}
